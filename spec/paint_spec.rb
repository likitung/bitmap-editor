require './lib/image'
require './lib/paint'

describe Paint do
  describe '.reset' do
    let(:image) { Image.new(4, 2) }
    let!(:paint) { Paint.new.pixel_coloring(image, 1, 1, 'P') }
    it 'should return a valid Image after pixel_coloring' do
      expect(image.field).to match_array([['P','O','O','O'], ['O','O','O','O']])
    end
  end

  describe '.batch_pixel_coloring' do
    let(:image) { Image.new(4, 2) }
    let(:array_of_coordinates) {[[1,1],[1,2]]}
    let!(:paint) { Paint.new.batch_pixel_coloring(image, array_of_coordinates, 'P') }
    it 'should return a valid Image after batch_pixel_coloring' do
      expect(image.field).to match_array([['P','P','O','O'], ['O','O','O','O']])
    end
  end
end