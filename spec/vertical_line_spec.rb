require './lib/image'
require './lib/paint'
require './lib/shapes/vertical_line'

describe Shapes::VerticalLine do
  describe '.draw' do
    let(:image) { Image.new(4, 2) }
    let!(:line)  { Shapes::VerticalLine.new(1, 1, 2, 'P').draw(image) }

    it 'should return a valid Image with painted line into P color' do
      expect(image.field).to match_array([['P','O','O','O'], ['P','O','O','O']])
    end

    context 'with wrong params' do
      it 'should return an error for wrong params type input' do
        expect { Shapes::VerticalLine.new('K', 1, 2,'P').draw(image) }.to raise_exception(ArgumentError)
        expect { Shapes::VerticalLine.new(1, 'K', 2, 'P').draw(image) }.to raise_exception(ArgumentError)
        expect { Shapes::VerticalLine.new(1, 1, 2,'KK').draw(image) }.to raise_exception(StandardError)
        expect { Shapes::VerticalLine.new(1, 1, 2,'d').draw(image) }.to raise_exception(StandardError)
        expect { Shapes::VerticalLine.new(1, 1, 2,[]).draw(image) }.to raise_exception(StandardError)
      end
    end

    context 'with invalid image class' do
      it 'should return an error for wrong class in draw' do
        expect { Shapes::VerticalLine.new(1, 1, 2,'P').draw(Paint.new) }.to raise_exception(StandardError)
      end
    end
  end
end
