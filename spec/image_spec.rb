require './lib/image'
require './lib/paint'
require './lib/shapes/dot'

describe Image do
  describe '.initialize' do
    it 'should return an error for x y integer wrong input' do
      expect { described_class.new(251, 250) }.to raise_exception(StandardError, 'Cols or Rows is more then 250 or less then 1')
      expect { described_class.new(5, 251) }.to raise_exception(StandardError, 'Cols or Rows is more then 250 or less then 1')
      expect { described_class.new(0, 25) }.to raise_exception(StandardError, 'Cols or Rows is more then 250 or less then 1')
      expect { described_class.new(25, 0) }.to raise_exception(StandardError, 'Cols or Rows is more then 250 or less then 1')
      expect { described_class.new(2, -1) }.to raise_exception(StandardError, 'Cols or Rows is more then 250 or less then 1')
      expect { described_class.new(-1, 2) }.to raise_exception(StandardError, 'Cols or Rows is more then 250 or less then 1')
    end

    it 'should return an error for x y String wrong input' do
      expect { described_class.new('K', 250) }.to raise_exception(ArgumentError)
      expect { described_class.new(24, 'K') }.to raise_exception(ArgumentError)
    end

    let(:image) { Image.new(4, 2) }

    it 'should return a valid Image for valid input' do
      expect(image.rows).to eq(4)
      expect(image.cols).to eq(2)
      expect(image.field).to match_array([['O','O','O','O'], ['O','O','O','O']])
    end
  end

  describe '.reset' do
    let(:image) { Image.new(4, 2) }
    it 'should return a valid Image after reset' do
      expect(image.reset).to match_array([['O','O','O','O'], ['O','O','O','O']])
    end
  end
end
