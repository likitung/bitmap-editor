require './lib/image'
require './lib/paint'
require './lib/shapes/horizontal_line'

describe Shapes::HorizontalLine do
  describe '.draw' do
    let(:image) { Image.new(4, 2) }
    let!(:line)  { Shapes::HorizontalLine.new(1, 2, 1, 'P').draw(image) }

    it 'should return a valid Image with painted line into P color' do
      expect(image.field).to match_array([['P','P','O','O'], ['O','O','O','O']])
    end

    context 'with wrong params' do
      it 'should return an error for -x and -y integer wrong input' do
        expect { Shapes::HorizontalLine.new(-1, 1, 1, 'P').draw(image) }.to raise_exception(StandardError, 'X is out of the image range')
        expect { Shapes::HorizontalLine.new(1, 1, -11,'P').draw(image) }.to raise_exception(StandardError, 'Y is out of the image range')
      end

      it 'should return an error for wrong params type input' do
        expect { Shapes::HorizontalLine.new('K', 1, 1,'P').draw(image) }.to raise_exception(ArgumentError)
        expect { Shapes::HorizontalLine.new(1, 'K', 1, 'P').draw(image) }.to raise_exception(ArgumentError)
        expect { Shapes::HorizontalLine.new(1, 1, 1,'KK').draw(image) }.to raise_exception(StandardError)
        expect { Shapes::HorizontalLine.new(1, 1, 1,'d').draw(image) }.to raise_exception(StandardError)
        expect { Shapes::HorizontalLine.new(1, 1, 1,[]).draw(image) }.to raise_exception(StandardError)
      end
    end

    context 'with invalid image class' do
      it 'should return an error for wrong class in draw' do
        expect { Shapes::HorizontalLine.new(1, 1, 1,'P').draw(Paint.new) }.to raise_exception(StandardError)
      end
    end
  end
end
