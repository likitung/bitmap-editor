require './lib/command_constructor'

describe CommandConstructor do
  describe '.create' do
    let(:constructor) { CommandConstructor.new.create(File.new('./spec/fixtures/show.txt')) }
    let(:final_array) { [
      {"name"=>"I", "parameters"=>{"rows"=>"6", "cols"=>"6"}},
      {"name"=>"L", "parameters"=>{"x"=>"1", "y"=>"1", "color"=>"A"}},
      {"name"=>"V", "parameters"=>{"x"=>"2", "y1"=>"1", "y2"=>"3", "color"=>"W"}},
      {"name"=>"H", "parameters"=>{"x1"=>"3", "x2"=>"5", "y"=>"2", "color"=>"Z"}},
      {"name"=>"S", "parameters"=>{} }
    ] }
    it 'should return a valid Image after pixel_coloring' do
      expect(constructor).to match_array(final_array)
    end
  end
end