# Bitmap editor

..

# Running

`>bin/bitmap_editor examples/show.txt`

# Requirements
- Ruby = 2.3

# Setup
- Goto project's folder in your terminal
- run `bundle install`
- setup `examples/show.txt` file with the commands
- now run `>bin/bitmap_editor examples/show.txt`

# Specs
if you want to run the specs please run `rspec .` in project's home directory.
