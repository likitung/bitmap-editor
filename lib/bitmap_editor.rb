require './lib/image'
require './lib/display'
require './lib/paint'
require './lib/shapes/horizontal_line'
require './lib/shapes/vertical_line'
require './lib/shapes/dot'
require './lib/command_constructor'

class BitmapEditor
  def run(file)
    commands_list = CommandConstructor.new.create(file)
    image = nil
    commands_list.each do |command|
      begin
        param = command['parameters']
        case command['name']
          when 'I'
            image = Image.new(param['rows'], param['cols'])
          when 'H'
            Shapes::HorizontalLine.new(param['x1'], param['x2'], param['y'], param['color']).draw(image)
          when 'V'
            Shapes::VerticalLine.new(param['x'], param['y1'], param['y2'], param['color']).draw(image)
          when 'S'
            Display.new(image).show
          when 'L'
            Shapes::Dot.new(param['x'], param['y'], param['color']).draw(image)
          when 'C'
            image = image.reset
          else
            raise StandardError, "Unknown Command: #{command['name']}"
        end
      rescue StandardError => error
        puts "Error: #{error.message}. Command: #{command}"
        break
      end
    end
  end
end
