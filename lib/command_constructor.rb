class CommandConstructor
  # Parse file and create proper commands list
  InvalidFile = Class.new(StandardError)
  NoSuchCommand = Class.new(StandardError)
  NoSuchIndex = Class.new(StandardError)

  def initialize; end

  def create(file)
    raise InvalidFile, 'Please provide correct file' if file.nil? || !File.exists?(file)
    generate_commands_list(file)
  end

  private

  def commands_pattern_list
    {
      'I' => [ 'rows', 'cols' ],
      'C' => [],
      'L' => [ 'x','y','color' ],
      'V' => [ 'x', 'y1', 'y2', 'color' ],
      'H' => [ 'x1','x2','y', 'color' ],
      'S' => []
    }
  end

  def generate_commands_list(file)
    commands = commands_pattern_list

    final_commands_list = []

    File.open(file).each do |line|
      params = line.chomp.upcase.split(' ')

      raise NoSuchCommand, 'No Command with that Key' unless commands.has_key?(params[0])

      command_name = params[0]
      params.delete(params[0])

      command_parameters = {}

      commands[command_name].each_with_index do |parameter_name, index|
        raise NoSuchCommand, 'No Such index' if params[index].nil?
        command_parameters.merge!(parameter_name => params[index])
      end

      final_commands_list << {
        'name' => command_name,
        'parameters' => command_parameters
      }
    end

    final_commands_list
  end
end