class Paint
  # Fill pixel or pixels with a color
  def initialize; end

  def pixel_coloring(image, x, y, color)
    raise Image::InvalidImage unless image.instance_of?(Image)
    image.paint_pixel(x, y, color)
  end

  def batch_pixel_coloring(image, array_of_coordinates, color)
    array_of_coordinates.each do |array|
      pixel_coloring(image, array[1], array[0], color)
    end
  end
end