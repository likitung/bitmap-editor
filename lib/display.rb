class Display
  # Display current image
  def initialize(image)
    @image = image
  end

  def show
    raise Image::InvalidImage unless @image.instance_of?(Image)
    @image.field.each do |array|
      puts array.each { |item| item }.join('')
    end
  end
end