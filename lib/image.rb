class Image
  # Create Image field
  InvalidImageSize = Class.new(StandardError)
  InvalidCoordinates = Class.new(StandardError)
  InvalidColor = Class.new(StandardError)
  InvalidImage = Class.new(StandardError)

  attr_reader :rows, :cols
  attr_accessor :field

  def initialize(rows, cols)
    @rows = Integer(rows)
    @cols = Integer(cols)
    validate_image_size
    create_field
  end

  def reset
    create_field
  end

  def paint_pixel(x, y, color)
    x = Integer(x)
    y = Integer(y)
    color = String(color)
    validate_pixel_position(x, y)
    validate_color(color)
    @field[y - 1][x - 1] = color
  end

  private

  def create_field
    @field = Array.new(@cols) { Array.new(@rows, 'O') }
  end

  def validate_image_size
    if @rows > 250 || @rows < 1 || @cols > 250 || @cols < 1
      raise InvalidImageSize, 'Cols or Rows is more then 250 or less then 1'
    end
  end

  def validate_pixel_position(x, y)
    raise InvalidCoordinates, 'Y is out of the image range' if y > @cols || y < 1
    raise InvalidCoordinates, 'X is out of the image range' if x > @rows || x < 1
  end

  def validate_color(color)
    raise InvalidColor, 'Only one capital letter allowed for color naming' if color.length > 1 || color.length < 1
    raise InvalidColor, 'Only one capital letter allowed for color naming' if /^[A-Z]/.match(color).nil?
  end
end