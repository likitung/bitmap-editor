module Shapes
  class Dot < Paint
    # Draw dot on the image
    def initialize(x, y, color)
      @x = Integer(x)
      @y = Integer(y)
      @color = String(color)
    end

    def draw(image)
      raise Image::InvalidImage unless image.instance_of?(Image)
      pixel_coloring(image, @x, @y, @color)
    end
  end
end
