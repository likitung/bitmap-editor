module Shapes
  class HorizontalLine < Paint
    # Draw Horizontal Line on the image
    InvalidParams = Class.new(StandardError)

    def initialize(x1, x2, y, color)
      @x1 = Integer(x1)
      @x2 = Integer(x2)
      @y = Integer(y)
      @color = String(color)
    end

    def draw(image)
      raise Image::InvalidImage unless image.instance_of?(Image)
      raise InvalidParams, 'X1 have to be more than X2 ' if @x1 > @x2

      diff = @x2 - @x1
      coordinates = [[@y, @x1]]
      diff.times do
        coordinates << [@y, @x1 + diff]
        diff = diff - 1
      end

      batch_pixel_coloring(image, coordinates, @color)
    end
  end
end
