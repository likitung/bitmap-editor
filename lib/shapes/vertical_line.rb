module Shapes
  class VerticalLine < Paint
    # Draw VerticalLine on the image
    InvalidParams = Class.new(StandardError)

    def initialize(x, y1, y2, color)
      @y1 = Integer(y1)
      @y2 = Integer(y2)
      @x = Integer(x)
      @color = String(color)
    end

    def draw(image)
      raise Image::InvalidImage unless image.instance_of?(Image)
      raise InvalidParams, 'Y1 have to be more than Y2 ' if @y1 > @y2

      diff = @y2 - @y1
      coordinates = [[ @y1, @x ]]
      diff.times do
        coordinates << [ @y1 + diff, @x ]
        diff = diff - 1
      end

      batch_pixel_coloring(image, coordinates, @color)
    end
  end
end
